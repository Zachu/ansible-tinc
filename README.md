Tinc
====

Hopefully there's something else here than quickstart in the future.

## Quickstart

Change these group variables to fit your needs and put them to `group_vars/tinc.yml` or `group_vars/all.yml` or other appropriate place.
```yaml
---
tinc_netname: tinc0
tinc_netmask: 255.255.255.0
tinc_network: 172.16.128.0/24
```

Assign each tinc node at least a `tinc_ip` in the defined network. Also one node at least has to have a `tinc_public` ip defined so other nodes can connect the network. Each defined tinc node tries to connect all other nodes that have `tinc_public` defined.
```
---
tinc_ip: 172.16.128.11
tinc_public: 172.16.1.11
```

Have fun :)
